using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringPremieTest
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void TestCalculationBasicPremium()
        {
            // Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var expectedPremium = ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5) / 3;

            // Act
            var actualValue = PremiumCalculation.CalculateBasePremium(vehicle);

            // Assert
            Assert.Equal(expectedPremium, actualValue);

        }

        [Fact]

        public void Storageyounger23Test()
        {
            //Arrange
            var vehicle = new Vehicle(100, 8000, 2015);

            var youngDriver = new PolicyHolder(18, "01-01-2006", 8000, 0);

            var oldDriver = new PolicyHolder(50, "01-01-2006", 8000, 0);

            // Act
            var premiumOldDriver = new PremiumCalculation(vehicle, oldDriver, InsuranceCoverage.WA);

            var premiumYoungDriver = new PremiumCalculation(vehicle, youngDriver, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumYoungDriver.PremiumAmountPerYear, premiumOldDriver.PremiumAmountPerYear * 1.15);
        }

        [Theory]
        [InlineData(5, false)]
        [InlineData(4, true)]

        public void StorageLess5YearTest(int driverAge, bool raise)
        {
            //Arrange
            var vehicle = new Vehicle(100, 8000, 2015);
            var startDateYoungDriver = DateTime.Now.AddYears(-driverAge).ToString();

            var youngDriver = new PolicyHolder(33, startDateYoungDriver, 8000, 0);

            var oldDriver = new PolicyHolder(33, "01-01-2006", 8000, 0);

            var premiumOldDriver = new PremiumCalculation(vehicle, oldDriver, InsuranceCoverage.WA);

            var expectedValue = raise
                ? premiumOldDriver.PremiumAmountPerYear * 1.15
                : premiumOldDriver.PremiumAmountPerYear;

            // Act
            var premiumYoungDriver = new PremiumCalculation(vehicle, youngDriver, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(expectedValue, premiumYoungDriver.PremiumAmountPerYear);
        }

        [Fact]
        public void SaveMailCode5Percent()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var mailCode5Percent = new PolicyHolder(33, "01-01-2006", 2200, 0);

            var mailCodeNone = new PolicyHolder(33, "01-01-2006", 8800, 0);

            //Act
            var premiumMailCode = new PremiumCalculation(vehicle, mailCode5Percent, InsuranceCoverage.WA);

            var premiumMailCodeNone = new PremiumCalculation(vehicle, mailCodeNone, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumMailCode.PremiumAmountPerYear, premiumMailCodeNone.PremiumAmountPerYear * 1.05);
        }

        [Fact]
        public void SaveMailcode2Percent()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var mailCode2Percent = new PolicyHolder(33, "01-01-2006", 4000, 0);

            var mailCodeNone = new PolicyHolder(33, "01-01-2006", 8800, 0);

            //Act
            var premieMailCode = new PremiumCalculation(vehicle, mailCode2Percent, InsuranceCoverage.WA);

            var premieMailCodeNone = new PremiumCalculation(vehicle, mailCodeNone, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premieMailCode.PremiumAmountPerYear, premieMailCodeNone.PremiumAmountPerYear * 1.02);
        }

        [Fact]
        public void WAPlus20Percent()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var driver = new PolicyHolder(34, "01-01-2006", 8800, 0);

            //Act 
            var waPremie = new PremiumCalculation(vehicle, driver, InsuranceCoverage.WA);

            var waPlus = new PremiumCalculation(vehicle, driver, InsuranceCoverage.WA_PLUS);

            // Assert 
            Assert.Equal(waPremie.PremiumAmountPerYear * 1.20, waPlus.PremiumAmountPerYear);

        }

        [Fact]
        public void allriskDouble()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var driver = new PolicyHolder(34, "01-01-2006", 8800, 0);

            //Act 
            var waPremie = new PremiumCalculation(vehicle, driver, InsuranceCoverage.WA);
            var waPlus = new PremiumCalculation(vehicle, driver, InsuranceCoverage.ALL_RISK);

            // Assert 
            Assert.Equal(waPremie.PremiumAmountPerYear * 2, waPlus.PremiumAmountPerYear);

        }

        [Theory]
        [InlineData(5,0)]
        [InlineData(6, 5)]
        [InlineData(17, 60)]
        [InlineData(19, 65)]
        [InlineData(36, 65)]

        public void NoDamageYears(int noClaimYears, double discountPercentage)
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var driverOne = new PolicyHolder(34, "01-01-2006", 8800, 0);
            var driverTwo = new PolicyHolder(34, "01-01-2006", 8800, noClaimYears);

            //Act 
            var damage  = new PremiumCalculation(vehicle, driverOne, InsuranceCoverage.WA);

            var noDamage = new PremiumCalculation(vehicle, driverTwo, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(noDamage.PremiumAmountPerYear, damage.PremiumAmountPerYear * (1- discountPercentage / 100));
        }

       [Fact]

       public void MonthlyPremium()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var driverOne = new PolicyHolder(34, "01-01-2006", 8800, 0);

            var premiumCalculator = new PremiumCalculation(vehicle, driverOne, InsuranceCoverage.WA);

            var expected = Math.Round(premiumCalculator.PremiumAmountPerYear / 12, 2);

            //Act
            var actualValue = premiumCalculator.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(expected, actualValue);
        }

        [Fact]

        public void YearlyPremium()
        {
            //Arrange 
            var vehicle = new Vehicle(100, 8000, 2015);

            var driverOne = new PolicyHolder(34, "01-01-2006", 8800, 0);

            var premiumCalculator = new PremiumCalculation(vehicle, driverOne, InsuranceCoverage.WA);

            var expected = Math.Round(premiumCalculator.PremiumAmountPerYear * (1 - 0.025), 2);

            //Act
            var actualValue = premiumCalculator.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.Equal(expected, actualValue);
        }

    }

}
