```
  _____          _____   _____ _    _ _____  ______ 
 / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|
| |       /  \  | |__) | (___ | |  | | |__) | |__   
| |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|  
| |____ / ____ \| | \ \ ____) | |__| | | \ \| |____ 
 \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|
```
 
#### Deel1:
-  Het doel van de unittest
-  Waarom heb je voor deze specifieke (data) invulling van de unittest hebt gekozen.
-  Welke technieken je gebruikt hebt om tot deze data te komen

#### De testen:
1. TestCalculationBasicPremium
2. Storageyounger23Test
3. StorageLess5YearTest
4. SaveMailCode5Percent
5. SaveMailcode2Percent
6. WAPlus20Percent
7. allriskDouble
8. NoDamageYears
9. MonthlyPremium
10. YearlyPremium

#### Uitleg:
1. TestCalculationBasicPremium <br>
Ik heb gekeken of de basispremie correct wordt berekend. <br>
Ik heb persoonlijk niet specifiek gekeken naar een auto meer naar waardes die binnen de grenzen vallen. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

2. Storageyounger23Test <br>
Bij deze unit test heb ik gekeken of de persoon ouder dan 23 is of jonger en of daar een verandering in zit van de premie. <br>
Bij deze test heb ik gebruik gemaakt van dezelfde gegevens als bij de voorgaande test. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

3. StorageLess5YearTest
Hier heb ik gekeken of de klant zijn/haar korter bezit dan 5 jaar. <br>
Hier voor de vehicle hetzelfde als voorgaande maar voor de bezit jaren waardes heb ik gekozen voor 4 en 5 jaar. <br>
Als techniek heb ik bij deze unit test gebruik gemaakt van [Theory].
<br>
<br>

4. SaveMailCode5Percent
Het doel was om te kijken of de persoon 5% opslag heeft of niet. <br>
Hiervoor heb ik een postcode gebruikt die binnen de 5% marge past. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

5. SaveMailcode2Percent
Het doel was om te kijken of de persoon 2% opslag heeft of niet. <br>
Hiervoor heb ik een postcode gebruikt die binnen de 2% marge past. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

6. WAPlus20Percent<br>
Hier heb ik gekeken of de InsuranceCoverage.WA_PLUS daadwerkelijk 26% duurder is ten opzichte van de normale WA. <br>
Hier heb ik de standaard informatie gebruikt om de vergelijking te maken tussen de WA en WA_PLUS.
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

7. allriskDouble<br>
Bij deze test heb ik gekeken of de ALL_RISK inderdaad het dubbelen was van de normale WA. <br>
Hier heb ik de WA vergeleken met de ALL_RISK en heb hiervoor dezelfde informatie meegegeven maar met andere insurance coverage. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

8. NoDamageYears<br>
Ik heb gekeken of de schadevrije jaren daadwerkelijk op een juiste manier doorlopen tot de 65% en hier dan vervolgens op blijft. <br>
De data die ik in de theorie heb gebruikt is als volgt,  [InlineData(5,0)] [InlineData(6, 5)] [InlineData(17, 60)] [InlineData(19, 65)] [InlineData(36, 65)] en met de korting die erbij hoort. <br>
Als techniek heb ik bij deze unit test gebruik gemaakt van [Theory].
<br>
<br>

9. MonthlyPremium<br>
Ik heb gekeken of de premie maandelijks correct is. <br>
Ik heb gebruik gemaakt van standaard informatie zonder enige toevoeging of extra's vervolgens heb ik het berekend van 1 jaar en daarna gedeeld door 12. En vervolgens is de afronding netjes met 2 decimalen. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

10. YearlyPremium<br>
Ik heb gekeken of de betaling na een jaar ook echt 2,5% korting geeft. <br>
Hier heb ik gebruik gemaakt van de standaard informatie zoals voorgaand benoemd. <br>
Als techniek  heb ik gebruik gemaakt van een [Fact].
<br>
<br>

### Deel 2:
- Wat heb je getest?
- Wat had je verwacht?
- Waar komt deze verwachting vandaan? 
<br>
<br>

### Deel 2 uitleg: 
De volgende fouten heb ik gevonden: 
```
if(policyHolder.Age < 23 || policyHolder.LicenseAge <= 5)
            {
                premium *= 1.15;
            }
```
Naar 
```
if(policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
            {
                premium *= 1.15;
            }
```
### Uitleg: 
- Gekeken of de Leeftijd groter is dan 23jaar en of de Licence Age daadwerkelijk kleiner is dan 5. <br>
- Zelf had ik verwacht dat de test zou falen omdat de test uitgevoerd wordt voor kleiner dan 5 en niet gewoon kleiner of eventueel hetzelfde.<br>
- De einde opdracht/resultaat was niet correct. 
<br/>
<br/>
<br/>

```
 private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((100 - NoClaimPercentage) / 100);
        }
```
Naar:

```
private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            int NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage > 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }
            return premium * ((double)(100 - NoClaimPercentage) / 100);
        }
```

### Uitleg:
- Een double toegevoegd, omdat het afrond en dan niet overeenkomt met de vergelijkende waarde in de test file.<br>
- Mijn verwachting was de het kloppent zou zijn zonder te weten dat de int niet goed stond. <br>
- De regel over het hoofd gezien.
<br/>
<br/>
<br/>

```
internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```

Naar:

```
internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear * (1 - 0.025) : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }
```
### Uitleg:
- De premie per jaar moetst getest worden alleen zat er een fout in de code waardoor deze niet goed berekend kon worden.<br>
- De fout over het hoofd gezien. <br>
- Verwachting was dat de code correct zou werken. 
<br/>
<br/>
<br/>

```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3;
        }
```

Naar:
```
internal static double CalculateBasePremium(Vehicle vehicle)
        {
            return ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5) / 3;
        }
```

### Uitleg: 
- Het premie bedrag moest netjes afgerond worden naar twee cijfers achter de komma maar de gegevens war een int dus dit klopte niet.<br>
- Er moest een double aangemaakt worden anders zou het niet werken. 
<br/>
<br/>
<br/>

